﻿using System;
using System.Collections.Generic;

namespace AlmostIncreasingSequenceCS
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] s1 = new int[4] {1, 3, 2, 1}; //false
            int[] s2 = new int[3]{1, 3, 2}; //true
            int[] s3 = new int[4]{1, 2, 1, 2}; // false
            int[] s4 = new int[6]{10, 1, 2, 3, 4, 5}; // true
            int[] s5 = new int[4]{0, -2, 5, 6}; // true
            int[] s6 = new int[5]{1, 2, 5, 3, 5}; // true
            System.Console.WriteLine(almostIncreasingSequence(s1));
            System.Console.WriteLine(almostIncreasingSequence(s2));
            System.Console.WriteLine(almostIncreasingSequence(s3));
            System.Console.WriteLine(almostIncreasingSequence(s4));
            System.Console.WriteLine(almostIncreasingSequence(s5));
            System.Console.WriteLine(almostIncreasingSequence(s6));
        }
        static bool almostIncreasingSequence(int[] sequence) {
            int preNum = int.MinValue;
            
            for (int i = 0; i < sequence.Length; i++)
            {
                int num = sequence[i];
                if (!(num > preNum)) 
                {
                    var index = i;
                    var seq1 = new List<int>(sequence);
                    seq1.RemoveAt(index);
                    if (isIncreasing(seq1)) 
                    {
                        return true;
                    }
                    if (index != sequence.Length -1) 
                    {
                        var seq2 = new List<int>(sequence);
                        seq2.RemoveAt(index+1);
                        if (isIncreasing(seq2)) 
                        {
                            return true;
                        }
                    }
                    if (index != 0) 
                    {
                        var seq3 = new List<int>(sequence);
                        seq3.RemoveAt(index-1);
                        if (isIncreasing(seq3)) 
                        {
                            return true;
                        }
                    }
                }
                preNum = num;
            }
            return false;
        }

        private static bool isIncreasing(List<int> sequence) {
            var preNum = int.MinValue;
            foreach (var num in sequence) {
                if (!(num > preNum)) {
                    return false;
                }
                preNum = num;
            }
            return true;
        }
    }
}
